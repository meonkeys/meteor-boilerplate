'use strict';

Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

Router.map(function() {
  return this.route('home', {
    path: '/'
  });
});

Router.onBeforeAction('loading');
